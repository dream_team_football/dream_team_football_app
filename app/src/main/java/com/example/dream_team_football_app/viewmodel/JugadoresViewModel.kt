package com.example.dream_team_football_app.viewmodel

import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.model.EstadisticasJugadores
import com.example.dream_team_football_app.model.Jugador
import com.example.dream_team_football_app.model.Usuario
import com.example.dream_team_football_app.api.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

var fragment = ""
val PREFS_NAME = "MyPrefsFile"


class JugadoresViewModel: ViewModel() {
    lateinit var repository: Repository
    var currentUsuario= MutableLiveData<Usuario>()

    var name=""
    var users= MutableLiveData<List<Usuario>>()
    var loginClean = false
    var data = MutableLiveData<List<Jugador>>()
    var estadisticas = MutableLiveData<List<EstadisticasJugadores>>()
    var currentJugador: Jugador? = null
    var currentUsuarioRanking: Usuario? = null
    val mercadoData = MutableLiveData<List<Jugador>?>()
    private val _valoracionesPorJugador = MutableLiveData<Map<Int, List<String>>>()
    val valoracionesPorJugador: LiveData<Map<Int, List<String>>> get() = _valoracionesPorJugador
    private var valoracionesActualizadas = false
    val success = MutableLiveData<Boolean>()
    var puntosCargados = MutableLiveData<Boolean>()
        private set
    val tusJugadores = MutableLiveData<List<Jugador>?>()
    var tusJugadoresAlinieadosList = MutableList(11) { Jugador(-1,1,"Añadir", "a", "Añadir", "a", "a", false,1, 1, 0,
        R.drawable.baseline_person_24.toString()) }
    var filteredPlayerList = MutableLiveData<List<Jugador>>()
    var filteredPlayerListSearchView = MutableLiveData<List<Jugador>>()
    val showToast: MutableLiveData<Boolean> = MutableLiveData()



    init {
        puntosCargados.value = false

    }

    fun fetchData(){
        success.postValue(false)
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getJugadores("/jugadores")
            withContext(Dispatchers.Main) {
                if (response.isSuccessful){
                    data.postValue(response.body())
                }
                else{
                    Log.e("Error:", response.message())
                }
            }
            success.postValue(true)
        }

    }

    fun fetchJugadoresRival() {
        success.postValue(false)
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getJugadoresIdUsuario(currentUsuarioRanking!!.usu_id)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    val jugadores = response.body()

                    val ordenPosiciones = listOf("PORTERO", "DEFENSA", "CENTROCAMPISTA", "DELANTERO")

                    val jugadoresOrdenados = jugadores?.sortedBy { ordenPosiciones.indexOf(it.posicion) }

                    tusJugadores.postValue(jugadoresOrdenados)
                } else {
                    Log.e("Error:", response.message())
                }
            }
            success.postValue(true)
        }
    }
    fun fetchJugadores() {
        success.postValue(false)
        CoroutineScope(Dispatchers.IO).launch {
                val response = repository.getJugadoresIdUsuario(currentUsuario.value!!.usu_id)
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    val jugadores = response.body()

                    val ordenPosiciones = listOf("PORTERO", "DEFENSA", "CENTROCAMPISTA", "DELANTERO")

                    val jugadoresOrdenados = jugadores?.sortedBy { ordenPosiciones.indexOf(it.posicion) }

                    tusJugadores.postValue(jugadoresOrdenados)
                } else {
                    Log.e("Error:", response.message())
                }
            }
            success.postValue(true)
        }
    }



    fun fetchMercadoData() {
        success.postValue(false)
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getJugadores("/jugadores")
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    val jugadores = response.body()

                    val jugadoresSinClub = jugadores?.filter { it.idUsuario == 2 }

                    val ordenPosiciones = listOf("DELANTERO", "CENTROCAMPISTA", "DEFENSA", "PORTERO")

                    val jugadoresOrdenados = jugadoresSinClub?.sortedBy { ordenPosiciones.indexOf(it.posicion) }

                    mercadoData.postValue(jugadoresOrdenados)
                } else {
                    Log.e("Error:", response.message())
                }
            }
            success.postValue(true)
        }
    }
    fun fetchPuntosJugadores() {
        if (!puntosCargados.value!!) {
            success.postValue(false)
            // Verifica si las valoraciones ya se han actualizado
            viewModelScope.launch {
                val response = repository.getEstadisticasJugadores("/estadisticasJugadores")
                if (response.isSuccessful) {
                    val estadisticasJugadores = response.body()

                    estadisticasJugadores?.let { it ->
                        val estadisticasPorJugador = it.groupBy { it.idJugador }

                        // Mapea las valoraciones de las tres jornadas más recientes para cada jugador
                        val ultimasValoracionesPorJugador = estadisticasPorJugador.mapValues { entry ->
                            entry.value.sortedByDescending { it.idJornada }
                                .take(3)
                                .map {  it.valoracion?.toString() ?: "-" }
                        }

                        Log.d("DEBUG", "MAPEOOOOOOO: $ultimasValoracionesPorJugador")

                        // Actualiza la variable _valoracionesPorJugador con las nuevas valoraciones
                        _valoracionesPorJugador.value = ultimasValoracionesPorJugador
                    }

                    // Marca las valoraciones como actualizadas
                    valoracionesActualizadas = true
                } else {
                    Log.e("Error:", response.message())
                }
                success.postValue(true)
            }

            // Marcar los puntos como cargados después de la carga exitosa
            puntosCargados.value = true
        }
    }

    fun fetchJornadas(){
        success.postValue(false)
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getEstadisticasIdJugadores("/estadisticasJugadores/${currentJugador?.idJugador}")
            withContext(Dispatchers.Main){
                if (response.isSuccessful){
                    val jornadas = response.body()
                    Log.d("DEBUG", "JORNADAAAS: $jornadas")

                    val jornadasOrdenadas = jornadas?.sortedBy { it.idJornada }
                    Log.d("AAAAA", "ORDENADAAAS: $jornadasOrdenadas")


                    estadisticas.postValue(jornadasOrdenadas!!)
                }else{
                    Log.e("Error:", response.message())
                }
            }
            success.postValue(true)
        }
    }

    fun updateIdUsuarioJugador() {
        val jugadorId = currentJugador?.idJugador
        Log.d("JUGADOR FUNCION", "${jugadorId}")

        val usuarioId = currentUsuario.value?.usu_id
        Log.d("USUARIO FUNCION", "${usuarioId}")

        if (jugadorId != null && usuarioId != null) {
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    // Llama a la función de la interfaz Retrofit para actualizar el idUsuario
                    repository.updateIdUsuarioJugador(jugadorId, usuarioId)
                } catch (e: Exception) {
                    Log.d("TRY CATCH FUNCION", "${e.message}")
                }
            }
        }
    }
    fun updateIdUsuarioJugadorToMaquina() {
        val jugadorId = currentJugador?.idJugador
        Log.d("JUGADOR FUNCION", "${jugadorId}")

        val usuarioId = 1

        if (jugadorId != null) {
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    // Llama a la función de la interfaz Retrofit para actualizar el idUsuario
                    repository.updateIdUsuarioJugador(jugadorId, usuarioId)
                    Log.d("USUARIO FUNCION", "${usuarioId}")

                } catch (e: Exception) {
                    Log.d("TRY CATCH FUNCION", "${e.message}")
                }
            }
        }
    }

    fun updateDineroUsuario(dinero: Int) {
        val idUsuario = currentUsuario.value?.usu_id
        if (idUsuario != null) {
            viewModelScope.launch(Dispatchers.IO) {
                try {
                    // Llama a la función de la interfaz Retrofit para actualizar el idUsuario
                    repository.updateDineroUsuario(idUsuario, dinero)
                } catch (e: Exception) {
                    Log.d("TRY CATCH FUNCION", "${e.message}")
                }
            }
        }
    }
    fun updatePassword (password: String) {
        val idUsuario = currentUsuario.value?.usu_id
        if (idUsuario != null){

            viewModelScope.launch(Dispatchers.IO) {
                try {
                    Log.d("CONTRASEÑAA2", idUsuario.toString())

                    // Llama a la función de la interfaz Retrofit para actualizar el idUsuario
                    repository.updatePassword(idUsuario, password)
                } catch (e: Exception) {
                    Log.d("TRY CATCH FUNCION", "${e.message}")
                }
            }
        }
    }

    fun updateJugadorAlineado(idJugador: Int, alineado: Boolean){
        viewModelScope.launch(Dispatchers.IO){
            try {
                repository.updateJugadorAlineado(idJugador, alineado)
            }catch (e: Exception){
                Log.d("TRY CATCH FUNCION", "${e.message}")
            }
        }

    }



    fun setSelectedJugador (jugador: Jugador) {
        currentJugador = jugador
    }
    fun setSelectedUser (user: Usuario) {
        currentUsuarioRanking  = user
    }

    fun getUsuario(username: String) {
        success.postValue(false)
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = repository.getUsuario("/usuario/$username")

                if (response.isSuccessful) {
                    if (Looper.myLooper() == Looper.getMainLooper()) {
                        currentUsuario.value = response.body()
                        success.postValue(true)
                    } else {
                        withContext(Dispatchers.Main) {
                            currentUsuario.value = response.body()
                            success.postValue(true)
                        }
                    }
                    Log.d("lista", "${currentUsuario.value}")
                } else {
                    Log.e("Error :", response.message())
                }
            } catch (e: Exception) {
                Log.e("Error", "Excepción en la corrutina: ${e.message}", e)
            }
        }
    }

    fun getJugadoresFiltered(liga: String, equipo: String, posicion: String) {
        success.postValue(false)

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = repository.getJugadoresFiltered("/jugadores/$liga/$equipo/$posicion")

                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        filteredPlayerList.postValue(response.body())
                    } else {
                        showToast.postValue(true)
                    }
                    success.postValue(true)
                }
            } catch (e: Exception) {
                Log.e("Error:", e.message ?: "Unknown error")
                success.postValue(true)
            }
        }
    }


    fun getUsers(){
        success.postValue(false)
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getUsers("/usuario")
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    val userList = response.body() ?: emptyList()
                    val sortedUsers = userList.sortedByDescending { it.puntosTotales }
                    users.postValue(sortedUsers)
                    success.postValue(true)
                }
                else{
                    Log.e("Error :", response.message())
                    success.postValue(true)

                }
            }
        }
    }

    fun getJugadorConMasPuntos(): Jugador? {
        return tusJugadores.value?.maxByOrNull { it.puntosTotales }
    }
    fun getJugadorMasCaro(): Jugador? {
        return tusJugadores.value?.maxByOrNull { it.precio }
    }

    fun findPlayerByName(name: String) {
        val originalList = filteredPlayerList.value ?: return
        val filteredList = mutableListOf<Jugador>()

        for (player in originalList) {
            if (player.nombre.lowercase().contains(name.lowercase())) {
                filteredList.add(player)
            }
        }

        filteredPlayerListSearchView.postValue(filteredList)
    }



}
