package com.example.dream_team_football_app.view.ayuda

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.FragmentAyuda2Binding
import com.example.dream_team_football_app.view.activities.MainActivity

class AyudaFragment2 : Fragment() {
    lateinit var binding: FragmentAyuda2Binding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAyuda2Binding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textsiguiente.setOnClickListener {
            findNavController().navigate(R.id.action_ayudaFragment2_to_ayudaFragment3)
        }
        binding.textomitir.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }
}