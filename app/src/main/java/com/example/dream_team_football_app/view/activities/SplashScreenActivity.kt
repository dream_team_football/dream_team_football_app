package com.example.dream_team_football_app.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.airbnb.lottie.LottieAnimationView
import android.content.Intent
import com.example.dream_team_football_app.R

class SplashScreenActivity : AppCompatActivity() {
    private val SPLASH_TIME_OUT: Long = 3000 // 3 segundos

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        // Obtén la referencia a la LottieAnimationView
        val lottieAnimationView = findViewById<LottieAnimationView>(R.id.lottieAnimationView)

        // Configura la animación utilizando el recurso Lottie JSON
        lottieAnimationView.setAnimation(R.raw.animationdtf) // Cambia esto al nombre de tu archivo Lottie JSON
        lottieAnimationView.playAnimation()

        // Utiliza un Handler para esperar el tiempo definido y luego iniciar la siguiente actividad
        Handler().postDelayed({
            // Aquí puedes iniciar la siguiente actividad
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish() // Cierra el SplashScreen para que no se vuelva a mostrar al volver atrás
        }, SPLASH_TIME_OUT)
    }
}