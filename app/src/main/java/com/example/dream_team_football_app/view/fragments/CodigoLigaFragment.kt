package com.example.dream_team_football_app.view.fragments

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.viewmodel.fragment
import com.google.android.material.textview.MaterialTextView

class CodigoLigaFragment : Fragment() {

    private lateinit var codigoIngresado: StringBuilder
    private lateinit var mensajeTextView: TextView
    private lateinit var imageViewCircle1: ImageView
    private lateinit var imageViewCircle2: ImageView
    private lateinit var imageViewCircle3: ImageView
    private lateinit var imageViewCircle4: ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_codigo_liga, container, false)

        // Inicializar variables
        codigoIngresado = StringBuilder()

        imageViewCircle1 = view.findViewById(R.id.imageview_circle1)
        imageViewCircle2 = view.findViewById(R.id.imageview_circle2)
        imageViewCircle3 = view.findViewById(R.id.imageview_circle3)
        imageViewCircle4 = view.findViewById(R.id.imageview_circle4)

        mensajeTextView = view.findViewById(R.id.wordTextView)

        // Asociar los botones del teclado a sus correspondientes IDs
        val teclado = view.findViewById<LinearLayout>(R.id.teclado)
        val button1 = view.findViewById<MaterialTextView>(R.id.button1)
        val button2 = view.findViewById<MaterialTextView>(R.id.button2)
        val button3 = view.findViewById<MaterialTextView>(R.id.button3)
        val button4 = view.findViewById<MaterialTextView>(R.id.button4)
        val button5 = view.findViewById<MaterialTextView>(R.id.button5)
        val button6 = view.findViewById<MaterialTextView>(R.id.button6)
        val button7 = view.findViewById<MaterialTextView>(R.id.button7)
        val button8 = view.findViewById<MaterialTextView>(R.id.button8)
        val button9 = view.findViewById<MaterialTextView>(R.id.button9)
        val button0 = view.findViewById<MaterialTextView>(R.id.button0)

        // Configurar el OnClickListener para cada botón del teclado
        button1.setOnClickListener { agregarNumero("1") }
        button2.setOnClickListener { agregarNumero("2") }
        button3.setOnClickListener { agregarNumero("3") }
        button4.setOnClickListener { agregarNumero("4") }
        button5.setOnClickListener { agregarNumero("5") }
        button6.setOnClickListener { agregarNumero("6") }
        button7.setOnClickListener { agregarNumero("7") }
        button8.setOnClickListener { agregarNumero("8") }
        button9.setOnClickListener { agregarNumero("9") }
        button0.setOnClickListener { agregarNumero("0") }

        // Configurar el OnClickListener para los botones Borrar y Comprobar
        val borrarButton = view.findViewById<MaterialTextView>(R.id.buttondelete)
        val comprobarButton = view.findViewById<MaterialTextView>(R.id.buttontick)
        val ayudaButton = view.findViewById<Button>(R.id.ayuda)

        borrarButton.setOnClickListener { borrarCodigo() }
        comprobarButton.setOnClickListener {
            comprobarCodigo()
        }

        ayudaButton.setOnClickListener {
            fragment = "codigoliga"
            findNavController().navigate(R.id.action_codigoLigaFragment_to_ayudaFragment1)
        }

        return view
    }

    private fun agregarNumero(numero: String) {
        if (codigoIngresado.length < 4) {
            codigoIngresado.append(numero)
        }
        actualizarImageViews()
    }

    private fun borrarCodigo() {
        actualizarMensaje("")
        if (codigoIngresado.isNotEmpty()) {
            codigoIngresado.deleteCharAt(codigoIngresado.length - 1)
        }
        actualizarImageViews()
    }

    private fun comprobarCodigo() {
        val codigoCorrecto = "1234"
        val resultadoCorrecto = codigoIngresado.toString() == codigoCorrecto

        if (resultadoCorrecto) {
            cambiarColorImageViews(R.color.verde)
            actualizarMensaje("Te has unido a la liga")
            Handler().postDelayed({
                findNavController().navigate(R.id.action_codigoLigaFragment_to_equipoFragment)
            }, 1000)
        } else {
            cambiarColorImageViews(R.color.rojo)
            actualizarMensaje("Código incorrecto")
            Handler().postDelayed({
                codigoIngresado.clear()
                actualizarMensaje("")
                actualizarImageViews()
                cambiarColorImageViews(R.color.oro)
            }, 1000)
        }
    }

    private fun cambiarColorImageViews(colorId: Int) {
        imageViewCircle1.setColorFilter(resources.getColor(colorId))
        imageViewCircle2.setColorFilter(resources.getColor(colorId))
        imageViewCircle3.setColorFilter(resources.getColor(colorId))
        imageViewCircle4.setColorFilter(resources.getColor(colorId))
    }

    private fun actualizarImageViews() {
        // Actualizar cada ImageView según la longitud del código
        actualizarImageView(imageViewCircle1, 0)
        actualizarImageView(imageViewCircle2, 1)
        actualizarImageView(imageViewCircle3, 2)
        actualizarImageView(imageViewCircle4, 3)
    }

    private fun actualizarImageView(imageView: ImageView, index: Int) {
        if (index < codigoIngresado.length) {
            // Mostrar el número correspondiente en el ImageView
            val numero = codigoIngresado[index].toString()
            // Aquí puedes asignar la imagen adecuada según el número
            imageView.setImageResource(R.drawable.cercle2)
            imageView.contentDescription = numero
        } else {
            // Si el índice está más allá de la longitud del código, limpiar el ImageView
            imageView.setImageResource(android.R.color.transparent)
            imageView.contentDescription = null
        }
    }
    fun actualizarMensaje(mensaje: String) {
        mensajeTextView.text = mensaje
        mensajeTextView.setTextColor(
            if (mensaje.contains("incorrecto")) resources.getColor(R.color.rojo)
            else resources.getColor(R.color.verde)
        )
    }
}
