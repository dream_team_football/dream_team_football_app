package com.example.dream_team_football_app.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.FragmentClasificacionBinding
import com.example.dream_team_football_app.adapters.UserAdapter
import com.example.dream_team_football_app.model.Usuario
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.JugadoresViewModel
import com.example.dream_team_football_app.listeners.OnClickListernerUsuarios
import com.example.dream_team_football_app.viewmodel.fragment
import java.text.NumberFormat
import java.util.Locale

class ClasificacionFragment : Fragment(), OnClickListernerUsuarios {

    lateinit var binding: FragmentClasificacionBinding
    private val viewModel: JugadoresViewModel by activityViewModels()
    private lateinit var userAdapter: UserAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentClasificacionBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)
        return binding.root
    }


    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.dinero.text = "${formatPrice(viewModel.currentUsuario.value?.dinero)}"

        // Mostrar ProgressBar antes de cargar los datos
        binding.progressBar.visibility = View.VISIBLE

        viewModel.success.observe(viewLifecycleOwner) { success ->
            if (success != null) {
                binding.progressBar.isVisible = !success
                binding.recyclerView.isVisible = success
                binding.recyclerView.setBackgroundColor(
                    if (success) R.color.black else android.R.color.transparent
                )
            }
        }
        viewModel.getUsers()

        viewModel.users.observe(viewLifecycleOwner) {

            setUpRecyclerView(it)
            // Ocultar ProgressBar después de cargar los datos
            binding.progressBar.visibility = View.GONE
        }
        Log.d("LISTA USERS 2", viewModel.users.value.toString())


        binding.perfil.setOnClickListener {
            findNavController().navigate(R.id.action_clasificacionFragment_to_userFragment2)
        }
        binding.ayuda.setOnClickListener {
            fragment = "clasificacion"
            findNavController().navigate(R.id.action_clasificacionFragment_to_ayudaFragment1)
        }
    }
    private fun setUpRecyclerView(listOfUsers: List<Usuario>){
        val filteredList = listOfUsers.filter { it.usu_id != 1 && it.usu_id != 2 }

        // Obtener el ID de usuario actual
        val currentUserId = viewModel.currentUsuario.value?.usu_id ?: -1

        // Crear el adaptador con la lista filtrada
        userAdapter = UserAdapter(filteredList, this, currentUserId)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }

    override fun onClickUser(user: Usuario) {
        if (viewModel.currentUsuario.value?.usu_id != user.usu_id){
            viewModel.setSelectedUser(user)
            findNavController().navigate(R.id.action_clasificacionFragment_to_equipoRivalFragment)
        }
    }

    fun formatPrice(price: Int?): String {
        val spanishLocale = Locale("es", "ES")
        val numberFormat = NumberFormat.getNumberInstance(spanishLocale)
        return numberFormat.format(price)
    }

}
