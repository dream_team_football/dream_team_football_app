package com.example.dream_team_football_app.view.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.FragmentMercadoBinding
import com.example.dream_team_football_app.adapters.Adapter
import com.example.dream_team_football_app.model.Jugador
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.JugadoresViewModel
import com.example.dream_team_football_app.listeners.OnClickListener
import com.example.dream_team_football_app.viewmodel.fragment
import java.text.NumberFormat
import java.util.Locale

class MercadoFragment : Fragment(), OnClickListener {

    lateinit var binding : FragmentMercadoBinding
    private val viewModel: JugadoresViewModel by activityViewModels()
    private lateinit var userAdapter: Adapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMercadoBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)
        return binding.root
    }

    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.dinero.text = "${formatPrice(viewModel.currentUsuario.value?.dinero)}"
        binding.perfil.setOnClickListener {
            findNavController().navigate(R.id.action_mercadoFragment_to_userFragment2)
        }
        binding.ayuda.setOnClickListener {
            fragment = "mercado"
            findNavController().navigate(R.id.action_mercadoFragment_to_ayudaFragment1)
        }

        viewModel.fetchMercadoData()

        viewModel.success.observe(viewLifecycleOwner) { success ->
            if (success != null) {
                binding.progressBar.isVisible = !success
                binding.recyclerView.isVisible = success
                binding.recyclerView.setBackgroundColor(
                    if (success) R.color.black else android.R.color.transparent
                )
            }
        }
        viewModel.fetchPuntosJugadores()

        viewModel.mercadoData.observe(viewLifecycleOwner) { mercadoData ->
            if (mercadoData != null) {

                setUpRecyclerView(mercadoData)
                viewModel.puntosCargados.observe(viewLifecycleOwner) { cargados ->
                    if (cargados) {
                        viewModel.puntosCargados.removeObservers(viewLifecycleOwner)
                    }
                }
                viewModel.valoracionesPorJugador.observe(viewLifecycleOwner) { valoracionesPorJugador ->
                    userAdapter.updateValoracionesPorJugador(valoracionesPorJugador)
                }

            }
        }
    }

    override fun OnButtonClicked(jugador: Jugador) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_comprarjugadores)

        val window = dialog.window
        window?.setBackgroundDrawableResource(R.drawable.fondo_perfilusuario)

        val btnSi = dialog.findViewById<Button>(R.id.btnSi)
        val btnNo = dialog.findViewById<Button>(R.id.btnNo)
        val textcomprar = dialog.findViewById<TextView>(R.id.comprarjugadortext)
        val textdinero = dialog.findViewById<TextView>(R.id.dinerojugadortext)

        textcomprar.text = "¿ESTÁS SEGURO QUE QUIERES COMPRAR A ${jugador.nombre.uppercase()} POR ESTE PRECIO?"
        textdinero.text = formatPrice(jugador.precio)

        btnSi.setOnClickListener {
            val customToastView = layoutInflater.inflate(R.layout.toast_jugadorcomprado, null)
            val toast = Toast(context)
            toast.duration = Toast.LENGTH_SHORT
            toast.view = customToastView
            toast.show()
            viewModel.setSelectedJugador(jugador)
            val dinero = viewModel.currentUsuario.value?.dinero

            Log.d("JUGADOR dinero", "${dinero}")

            val precioJugador = jugador.precio

            Log.d("JUGADOR precioJugador", "${precioJugador}")

            val resta = dinero?.minus(precioJugador)

            if (resta != null) {
                if (resta>=0){
                    viewModel.updateDineroUsuario(resta)
                    viewModel.updateIdUsuarioJugador()
                    Log.d("RESTA", "${resta}")
                    viewModel.currentUsuario.value?.dinero = resta
                    binding.dinero.text = "${formatPrice(viewModel.currentUsuario.value?.dinero)}"
                }else{
                    val customToastView = layoutInflater.inflate(R.layout.toast_notienesdinero, null)
                    val toast = Toast(context)
                    toast.duration = Toast.LENGTH_SHORT
                    toast.view = customToastView
                    toast.show()
                }
            }
            Log.d("JUGADOR ACUALIZADO", "${viewModel.currentJugador?.idJugador}")

            dialog.dismiss()
        }
        btnNo.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    private fun setUpRecyclerView(listOfUsers: List<Jugador>) {
        userAdapter = Adapter(listOfUsers, this, isFromMercadoFragment = true, isFromListaJugadores = false, isFromEquipoFragment = false, isFromEquipoRivalFragment = false)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }
    override fun onClick(jugador: Jugador) {
        viewModel.setSelectedJugador(jugador)
        val navController = findNavController()
        navController.navigate(R.id.action_mercadoFragment_to_detailJugadorFragment)
    }

    fun formatPrice(price: Int?): String {
        val spanishLocale = Locale("es", "ES")
        val numberFormat = NumberFormat.getNumberInstance(spanishLocale)
        return numberFormat.format(price)
    }

}