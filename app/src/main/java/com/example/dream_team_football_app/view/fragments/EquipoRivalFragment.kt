package com.example.dream_team_football_app.view.fragments

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dream_team_football_app.R

import com.example.dream_team_football_app.databinding.FragmentEquipoRivalBinding
import com.example.dream_team_football_app.adapters.Adapter
import com.example.dream_team_football_app.model.Jugador
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.JugadoresViewModel
import com.example.dream_team_football_app.listeners.OnClickListener
import java.text.NumberFormat
import java.util.Locale


class EquipoRivalFragment : Fragment(), OnClickListener {
    lateinit var binding: FragmentEquipoRivalBinding
    private val viewModel: JugadoresViewModel by activityViewModels()
    private lateinit var equipoAdapter: Adapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentEquipoRivalBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)
        Log.d("EQUIPORIVAlFRAGMENT", viewModel.currentUsuario.value.toString())
        binding.header.text = "Equipo de ${viewModel.currentUsuarioRanking?.usu_username}"

        // Mostrar el ProgressBar
        binding.progressBar.visibility = View.VISIBLE

        viewModel.success.observe(viewLifecycleOwner) { success ->
            if (success != null) {
                binding.progressBar.isVisible = !success
                binding.recyclerView.isVisible = success
            }
        }

        viewModel.fetchJugadoresRival()

        viewModel.tusJugadores.observe(viewLifecycleOwner) { jugadores ->
            if (jugadores != null) {
                setUpRecyclerView(jugadores)
                binding.progressBar.visibility = View.GONE
            }
        }
        Log.d("LISTA JUGADORES", "${viewModel.tusJugadores.value}")
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.volveratras.setOnClickListener {
            findNavController().navigate(R.id.action_equipoRivalFragment_to_clasificacionFragment)
        }
    }
    private fun setUpRecyclerView(listOfUsers: List<Jugador>){
        equipoAdapter = Adapter(listOfUsers, this, isFromMercadoFragment = false, isFromListaJugadores = false, isFromEquipoFragment = false, isFromEquipoRivalFragment = true)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = equipoAdapter
        }
        viewModel.puntosCargados.observe(viewLifecycleOwner) { cargados ->
            if (cargados) {
                viewModel.puntosCargados.removeObservers(viewLifecycleOwner)
            }
        }
        viewModel.valoracionesPorJugador.observe(viewLifecycleOwner) { valoracionesPorJugador ->
            equipoAdapter.updateValoracionesPorJugador(valoracionesPorJugador)
        }
    }
    override fun onClick(jugador: Jugador) {
        viewModel.setSelectedJugador(jugador)
        val navController = findNavController()
        navController.navigate(R.id.action_equipoRivalFragment_to_detailJugadorFragment)
    }

    override fun OnButtonClicked(jugador: Jugador) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_clausulazo)
        val window = dialog.window
        window?.setBackgroundDrawableResource(R.drawable.fondo_perfilusuario)

        val btnSi = dialog.findViewById<Button>(R.id.btnSi)
        val btnNo = dialog.findViewById<Button>(R.id.btnNo)
        val textcomprar = dialog.findViewById<TextView>(R.id.comprarjugadortext)
        val textdinero = dialog.findViewById<TextView>(R.id.dinerojugadortext)

        textcomprar.text = "¿ESTÁS SEGURO QUE QUIERES HACER EL CLAUSULAZO A ${jugador.nombre.uppercase()} ?"
        textdinero.text = formatPrice(jugador.precioClausula)
        btnSi.setOnClickListener {
            val customToastView = layoutInflater.inflate(R.layout.toast_clausulazo, null)
            val toast = Toast(context)
            toast.duration = Toast.LENGTH_SHORT
            toast.view = customToastView
            toast.show()
            viewModel.setSelectedJugador(jugador)

            val dinero = viewModel.currentUsuario.value?.dinero

            Log.d("JUGADOR dinero", "${dinero}")

            val precioClausulaJugador = jugador.precioClausula

            Log.d("JUGADOR precioJugador", "${precioClausulaJugador}")

            val resta = dinero?.minus(precioClausulaJugador)

            if (resta != null) {
                if (resta>=0){
                    viewModel.updateDineroUsuario(resta)
                    viewModel.updateIdUsuarioJugador()
                    Log.d("RESTA", "${resta}")
                    viewModel.currentUsuario.value?.dinero = resta
                    viewModel.updateJugadorAlineado(jugador.idJugador, false)
                }else{
                    val customToastView = layoutInflater.inflate(R.layout.toast_notienesdinero, null)
                    val toast = Toast(context)
                    toast.duration = Toast.LENGTH_SHORT
                    toast.view = customToastView
                    toast.show()
                }
            }
            Log.d("JUGADOR ACUALIZADO", "${viewModel.currentJugador?.idJugador}")

            dialog.dismiss()
        }
        btnNo.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

    }

    private fun formatPrice(price: Int?): String {
        val spanishLocale = Locale("es", "ES")
        val numberFormat = NumberFormat.getNumberInstance(spanishLocale)
        return numberFormat.format(price)
    }


}