package com.example.dream_team_football_app.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.api.Repository
import com.example.dream_team_football_app.databinding.FragmentDetailJugadorBinding
import com.example.dream_team_football_app.adapters.AdapterEstadisticas
import com.example.dream_team_football_app.model.EstadisticasJugadores
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.JugadoresViewModel
import com.example.dream_team_football_app.listeners.OnClickListenerJornadas
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.NumberFormat
import java.util.Locale

class DetailJugadorFragment : Fragment(), OnClickListenerJornadas {

    private lateinit var repository: Repository
    lateinit var binding : FragmentDetailJugadorBinding
    private val viewModel: JugadoresViewModel by activityViewModels()
    private lateinit var estadisiticasAdapter: AdapterEstadisticas
    var paginaprevia = ""

    @SuppressLint("ResourceAsColor")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDetailJugadorBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)

        repository = Repository(viewModel.currentUsuario.value!!.usu_username, viewModel.currentUsuario.value!!.usu_password)

        viewModel.fetchJornadas()

        viewModel.success.observe(viewLifecycleOwner) { success ->
            if (success != null) {
                binding.progressBar.isVisible = !success
                binding.recyclerViewJornadas.isVisible = success
                binding.chargeView.isVisible = !success

                binding.recyclerViewJornadas.setBackgroundColor(
                    if (success) R.color.black else android.R.color.transparent
                )

            }
        }

        viewModel.estadisticas.observe(viewLifecycleOwner){
            setUpRecyclerView(it)
        }

        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.nameTextView.text = viewModel.currentJugador?.nombre
        binding.valorMercadoTextView.text = ("Mercado: ${formatPrice(viewModel.currentJugador?.precio)}")
        binding.puntosTotalesTextView.text = ("${viewModel.currentJugador?.puntosTotales.toString()} pts")
        binding.valorClausulaTextView.text = ("Cláusula: ${formatPrice(viewModel.currentJugador?.precioClausula)}")
        binding.shortNameTextView.text =viewModel.currentJugador?.shortName.toString()
        updateEscudoTextView(viewModel.currentJugador?.equipo.toString())
        updatePosicion(viewModel.currentJugador?.posicion.toString())
        binding.backImageview.setOnClickListener {
            requireActivity().onBackPressed()
        }

        viewModel.estadisticas.observe(viewLifecycleOwner) { listaEstadisticas ->
            if (listaEstadisticas.isNotEmpty()) {
                val ultimaEstadistica = listaEstadisticas[listaEstadisticas.size - 1]

                binding.minutosValorTextView.text = ultimaEstadistica.minutos?.toString()?: "-"
                binding.golesValorTextView.text = ultimaEstadistica.goles?.toString()?: "-"
                binding.asitenciasValorTextView.text = ultimaEstadistica.asistencias?.toString()?: "-"
                binding.paradasValorTextView.text = ultimaEstadistica.paradas?.toString()?: "-"
                binding.tirosValorTextView.text = ultimaEstadistica.tirosAPuerta?.toString()?: "-"
                binding.perdidosValorTextView.text = ultimaEstadistica.balonesPerdidos?.toString()?: "-"
                binding.puntosValorTextView.text = ultimaEstadistica.valoracion?.toString()?: "-"
            }
        }


        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getImage("/jugadores/imagenes/${viewModel.currentJugador?.nombre}.webp")
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && response.body() != null) {
                    val foto = response.body()!!.bytes()
                    context?.let {
                        Glide.with(it)
                            .load(foto)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .centerCrop()
                            .into(binding.foto)
                    }
                }
            }
        }
    }

    private fun setUpRecyclerView(listofJornada: List<EstadisticasJugadores>) {
        estadisiticasAdapter = AdapterEstadisticas(listofJornada, this)
        val horizontalLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

        binding.recyclerViewJornadas.apply {
            setHasFixedSize(true)
            layoutManager = horizontalLayoutManager
            adapter = estadisiticasAdapter

            val lastIndex = listofJornada.size - 1

            horizontalLayoutManager.scrollToPosition(lastIndex)
        }
    }
    fun updatePosicion(posicion: String) {
        when (posicion) {
            "DELANTERO" -> {
                binding.posicionTextview.text = "DL"
                binding.posicionTextview.setBackgroundResource(R.drawable.border_position_dl)
            }
            "CENTROCAMPISTA" -> {
                binding.posicionTextview.text = "MC"
                binding.posicionTextview.setBackgroundResource(R.drawable.border_position_mc)
            }
            "DEFENSA" -> {
                binding.posicionTextview.text = "DF"
                binding.posicionTextview.setBackgroundResource(R.drawable.border_position_df)
            }
            "PORTERO" -> {
                binding.posicionTextview.text = "POR"
                binding.posicionTextview.setBackgroundResource(R.drawable.border_position_por)
            }
        }
    }
    fun updateEscudoTextView (escudo: String) {
        when (escudo) {
            "ATHLETIC CLUB" -> {
                binding.escudoImageview.setImageResource(R.drawable.atlethic_club)
            }
            "OSASUNA" -> {
                binding.escudoImageview.setImageResource(R.drawable.osasuna)
            }
            "ATLETICO" -> {
                binding.escudoImageview.setImageResource(R.drawable.atleticomadrid)
            }
            "CADIZ" -> {
                binding.escudoImageview.setImageResource(R.drawable.cadiz)
            }
            "ALAVES" -> {
                binding.escudoImageview.setImageResource(R.drawable.alaves)
            }
            "SEVILLA" -> {
                binding.escudoImageview.setImageResource(R.drawable.sevilla)
            }
            "VALENCIA" -> {
                binding.escudoImageview.setImageResource(R.drawable.valencia)
            }
            "GRANADILLA" -> {
                binding.escudoImageview.setImageResource(R.drawable.granadilla)
            }
        }
    }


    override fun onClick(jornada: EstadisticasJugadores){

        Log.d("AAAAAAA", "${jornada.minutos}")

        binding.minutosValorTextView.text = jornada.minutos?.toString()?: "-"
        binding.golesValorTextView.text = jornada.goles?.toString()?: "-"
        binding.asitenciasValorTextView.text = jornada.asistencias?.toString()?: "-"
        binding.paradasValorTextView.text = jornada.paradas?.toString()?: "-"
        binding.tirosValorTextView.text = jornada.tirosAPuerta?.toString()?: "-"
        binding.perdidosValorTextView.text = jornada.balonesPerdidos?.toString()?: "-"
        binding.puntosValorTextView.text = jornada.valoracion?.toString()?: "-"

    }
    fun formatPrice(price: Int?): String {
        val spanishLocale = Locale("es", "ES")
        val numberFormat = NumberFormat.getNumberInstance(spanishLocale)
        return numberFormat.format(price)
    }
}