package com.example.dream_team_football_app.view.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.FragmentJugadoresListaBinding
import com.example.dream_team_football_app.adapters.Adapter
import com.example.dream_team_football_app.model.Jugador
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.JugadoresViewModel
import com.example.dream_team_football_app.listeners.OnClickListener
import com.example.dream_team_football_app.viewmodel.fragment
import java.text.NumberFormat
import java.util.Locale
import androidx.appcompat.widget.SearchView

class JugadoresListaFragment : Fragment(), OnClickListener {
    lateinit var binding: FragmentJugadoresListaBinding
    private val viewModel: JugadoresViewModel by activityViewModels()
    private lateinit var userAdapter: Adapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private var arePlayersStored = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentJugadoresListaBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)
        return binding.root
    }

    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.dinero.text = "${formatPrice(viewModel.currentUsuario.value?.dinero)}"
        binding.perfil.setOnClickListener {
            findNavController().navigate(R.id.action_jugadoresListaFragment_to_userFragment2)
        }
        viewModel.showToast.observe(viewLifecycleOwner) { shouldShowToast ->
            if (shouldShowToast) {
                val customToastView = layoutInflater.inflate(R.layout.toast_jugadores_no_encontrados, null)
                val toast = Toast(context)
                toast.duration = Toast.LENGTH_SHORT
                toast.view = customToastView
                toast.show()
                viewModel.showToast.postValue(false) // Resetea el valor para evitar que se muestre repetidamente
            }
        }
        binding.ayuda.setOnClickListener {
            fragment = "listajugadores"
            findNavController().navigate(R.id.action_jugadoresListaFragment_to_ayudaFragment1)
        }
        viewModel.fetchData()
        viewModel.success.observe(viewLifecycleOwner) {success ->
            if (success != null){
                binding.progressBar.isVisible = !success
                binding.recyclerView.isVisible = success
                binding.recyclerView.setBackgroundColor(
                    if (success) R.color.black else android.R.color.transparent
                )
            }
        }
        viewModel.fetchPuntosJugadores()
        viewModel.data.observe(viewLifecycleOwner) { data ->
            if (data != null) {

                arePlayersStored = true
                setUpRecyclerView(data)


            }
        }
        val spinnerLiga: Spinner = binding.spinnerLiga
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.ligas_array,
            R.layout.spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerLiga.adapter = adapter
        }
        val spinnerEquipo: Spinner = binding.spinnerEquipo
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.equipos_array,
            R.layout.spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerEquipo.adapter = adapter
        }

        val spinnerPosicion: Spinner = binding.spinnerPosicion
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.posiciones_array,
            R.layout.spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerPosicion.adapter = adapter
        }

        binding.searchEditText.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrBlank()) {
                    if (arePlayersStored) {
                        setUpRecyclerView(viewModel.filteredPlayerList.value!!)
                    }
                } else {
                    viewModel.findPlayerByName(newText.toString())

                    if (viewModel.filteredPlayerListSearchView.value != null){
                        setUpRecyclerView(viewModel.filteredPlayerListSearchView.value!!)
                    }
                }
                return false
            }
        })

        binding.spinnerLiga.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val liga = when (val ligaSeleccionada = binding.spinnerLiga.selectedItem.toString()) {
                    "Liga F" -> "LigaF"
                    else -> ligaSeleccionada
                }
                val equipo = when(val equipoSeleccionado = binding.spinnerEquipo.selectedItem.toString()){
                    "Athletic Club" -> "ATHLETIC CLUB"
                    "Atlético de Madrid" -> "ATLETICO"
                    "CA Osasuna" -> "OSASUNA"
                    "Cádiz CF" -> "CADIZ"
                    "Deportivo Alavés" -> "ALAVES"
                    "Sevilla FC" -> "SEVILLA"
                    "UD Granadilla Tenerife" -> "GRANADILLA"
                    "Valencia CF" -> "VALENCIA"
                    else -> equipoSeleccionado
                }
                val posicion = binding.spinnerPosicion.selectedItem.toString()

                viewModel.getJugadoresFiltered(liga, equipo, posicion.uppercase())

                viewModel.filteredPlayerList.observe(viewLifecycleOwner) { jugadores ->
                    setUpRecyclerView(jugadores)
                }

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        binding.spinnerEquipo.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val liga = when (val ligaSeleccionada = binding.spinnerLiga.selectedItem.toString()) {
                    "Liga F" -> "LigaF"
                    else -> ligaSeleccionada
                }
                val equipo = when(val equipoSeleccionado = binding.spinnerEquipo.selectedItem.toString()){
                    "Athletic Club" -> "ATHLETIC CLUB"
                    "Atlético de Madrid" -> "ATLETICO"
                    "CA Osasuna" -> "OSASUNA"
                    "Cádiz CF" -> "CADIZ"
                    "Deportivo Alavés" -> "ALAVES"
                    "Sevilla FC" -> "SEVILLA"
                    "UD Granadilla Tenerife" -> "GRANADILLA"
                    "Valencia CF" -> "VALENCIA"

                    else -> equipoSeleccionado
                }
                val posicion = binding.spinnerPosicion.selectedItem.toString()
                viewModel.getJugadoresFiltered(liga, equipo, posicion.uppercase())

                viewModel.filteredPlayerList.observe(viewLifecycleOwner) { jugadores ->
                    setUpRecyclerView(jugadores)
                }

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        binding.spinnerPosicion.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val liga = when (val ligaSeleccionada = binding.spinnerLiga.selectedItem.toString()) {
                    "Liga F" -> "LigaF"
                    else -> ligaSeleccionada
                }
                val equipo = when(val equipoSeleccionado = binding.spinnerEquipo.selectedItem.toString()){
                    "Athletic Club" -> "ATHLETIC CLUB"
                    "Atlético de Madrid" -> "ATLETICO"
                    "CA Osasuna" -> "OSASUNA"
                    "Cádiz CF" -> "CADIZ"
                    "Deportivo Alavés" -> "ALAVES"
                    "Sevilla FC" -> "SEVILLA"
                    "UD Granadilla Tenerife" -> "GRANADILLA"
                    "Valencia CF" -> "VALENCIA"

                    else -> equipoSeleccionado
                }
                val posicion = binding.spinnerPosicion.selectedItem.toString()
                viewModel.getJugadoresFiltered(liga, equipo, posicion.uppercase())

                viewModel.filteredPlayerList.observe(viewLifecycleOwner) { jugadores ->
                    setUpRecyclerView(jugadores)
                }

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

    }

    private fun setUpRecyclerView(listJugadores: List<Jugador>){
        val jugadoresOrdenados = listJugadores?.sortedByDescending { (it.puntosTotales) }

        userAdapter = Adapter(jugadoresOrdenados!! , this, isFromMercadoFragment = false, isFromListaJugadores = true, isFromEquipoFragment = false, isFromEquipoRivalFragment = false)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
        viewModel.puntosCargados.observe(viewLifecycleOwner) { cargados ->
            if (cargados) {
                viewModel.puntosCargados.removeObservers(viewLifecycleOwner)
            }
        }
        viewModel.valoracionesPorJugador.observe(viewLifecycleOwner) { valoracionesPorJugador ->
            userAdapter.updateValoracionesPorJugador(valoracionesPorJugador)
        }

    }
    override fun onClick(jugador: Jugador) {
        viewModel.setSelectedJugador(jugador)
        val navController = findNavController()
        navController.navigate(R.id.action_jugadoresListaFragment_to_detailJugadorFragment)
    }

    override fun OnButtonClicked(jugador: Jugador) {
        viewModel.setSelectedJugador(jugador)
    }


    fun formatPrice(price: Int?): String {
        val spanishLocale = Locale("es", "ES")
        val numberFormat = NumberFormat.getNumberInstance(spanishLocale)
        return numberFormat.format(price)
    }
}