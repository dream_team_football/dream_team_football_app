package com.example.dream_team_football_app.view.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController

import androidx.navigation.ui.setupWithNavController

import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.ActivityMainBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var bottomNavigationView: BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        bottomNavigationView = binding.bottomNavigation
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController
        bottomNavigationView.setupWithNavController(navController)

        setSupportActionBar(findViewById(R.id.toolbar))

        val appBarConfiguration = AppBarConfiguration(setOf(R.id.mercadoFragment, R.id.clasificacionFragment, R.id.equipoFragment, R.id.jugadoresListaFragment))
        setupActionBarWithNavController(navController, appBarConfiguration)


    }

    fun setBottomNavigationVisible(visible: Boolean) {
        if (visible) {
            bottomNavigationView.visibility = View.VISIBLE
            bottomNavigationView.setBackgroundResource(R.drawable.backgroundmenu)
        } else {
            bottomNavigationView.visibility = View.GONE
            bottomNavigationView.setBackgroundResource(R.drawable.backgroundmenu)
        }
    }
}