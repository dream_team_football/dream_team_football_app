package com.example.dream_team_football_app.view.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.FragmentEquipoBinding
import com.example.dream_team_football_app.databinding.JugadorPlantillaItemBinding
import com.example.dream_team_football_app.adapters.Adapter
import com.example.dream_team_football_app.model.Jugador
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.JugadoresViewModel
import com.example.dream_team_football_app.listeners.OnClickListener
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.text.NumberFormat
import java.util.Locale

class EquipoFragment : Fragment(), OnClickListener {

    lateinit var binding: FragmentEquipoBinding
    private val viewModel: JugadoresViewModel by activityViewModels()
    private lateinit var equipoAdapter: Adapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var posicionList: List<JugadorPlantillaItemBinding>
    private lateinit var listaParaUsar: MutableList<Jugador>



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentEquipoBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)
        Log.d("EQUIPOFRAGMENT", viewModel.currentUsuario.value.toString())

        posicionList = listOf(binding.portero, binding.lateralizquierdo, binding.defensaIzquierdo, binding.defensaDerecho, binding.lateralderecho, binding.centrocampistaIzquierdo, binding.centrocampista, binding.centrocampistaDerecho, binding.extremoDerecho, binding.delantero, binding.extremoIzquierdo)

        val listaJugadores = obtenerLista(requireContext(), "jugadores_alineados")

        listaParaUsar = listaJugadores ?: viewModel.tusJugadoresAlinieadosList

        viewModel.tusJugadoresAlinieadosList = listaParaUsar




        viewModel.fetchJugadores()
        viewModel.success.observe(viewLifecycleOwner) {success ->
            if (success != null){
                binding.progressBar.isVisible = !success

            }
        }

        viewModel.fetchPuntosJugadores()
        viewModel.tusJugadores.observe(viewLifecycleOwner) { jugadores ->
            if (jugadores != null) {
                setUpRecyclerView(jugadores)
                viewModel.puntosCargados.observe(viewLifecycleOwner) { cargados ->
                    if (cargados) {
                        viewModel.puntosCargados.removeObservers(viewLifecycleOwner)
                    }
                }
                viewModel.valoracionesPorJugador.observe(viewLifecycleOwner) {valoracionPorJugador ->
                    equipoAdapter.updateValoracionesPorJugador(valoracionPorJugador)
                }
            }
        }

        // Llamar a la función para obtener los jugadores
        Log.d("LISTA JUGADORES", "${viewModel.tusJugadores.value}")
        return binding.root
    }

    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("USUARIO ONVIEWCREATED", "${viewModel.currentUsuario.value?.usu_id}")


        for (i in 0..posicionList.lastIndex){
            posicionList[i].playerName.text = viewModel.tusJugadoresAlinieadosList[i].shortName

            Glide.with(this)
                .load(viewModel.tusJugadoresAlinieadosList[i].imagenUrl)
                .apply(RequestOptions().circleCrop())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(posicionList[i].playerImage)

            posicionList[i].puntuacion.text = viewModel.tusJugadoresAlinieadosList[i].puntosTotales.toString()

            posicionList[i]
        }

        binding.portero.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "PORTERO"
            val indice = 0

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)

            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }

        binding.lateralizquierdo.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "DEFENSA"
            val indice = 1

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)

            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }

        binding.defensaIzquierdo.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "DEFENSA"
            val indice = 2

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)

            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }


        binding.defensaDerecho.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "DEFENSA"
            val indice = 3

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)

            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }


        binding.lateralderecho.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "DEFENSA"
            val indice = 4

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)

            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }

        binding.centrocampistaIzquierdo.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "CENTROCAMPISTA"
            val indice = 5

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)

            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }


        binding.centrocampista.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "CENTROCAMPISTA"
            val indice = 6

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)

            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }

        binding.centrocampistaDerecho.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "CENTROCAMPISTA"
            val indice = 7

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)

            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }

        binding.extremoIzquierdo.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "DELANTERO"
            val indice = 10

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)


            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }


        binding.delantero.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "DELANTERO"
            val indice = 9

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)


            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }

        binding.extremoDerecho.jugadorPlantillaItem.setOnClickListener {
            val nextFragment = JugadoresListaFragment()

            val bundle = Bundle()
            val tuString = "DELANTERO"
            val indice = 8

            bundle.putString("posicion", tuString)
            bundle.putInt("indice", indice)


            nextFragment.arguments = bundle

            findNavController().navigate(R.id.action_equipoFragment_to_elegirJugadorFragment, bundle)
        }

    }

    private fun setUpRecyclerView(listJugadores: List<Jugador>){
        equipoAdapter = Adapter(listJugadores, this, isFromMercadoFragment = false, isFromListaJugadores = false, isFromEquipoFragment = false, isFromEquipoRivalFragment = false)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerViewEquipo.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = equipoAdapter
        }
    }
    override fun onClick(jugador: Jugador) {
        viewModel.setSelectedJugador(jugador)
        val navController = findNavController()
        navController.navigate(R.id.action_equipoFragment_to_detailJugadorFragment)
    }

    @SuppressLint("SuspiciousIndentation")
    override fun OnButtonClicked(jugador: Jugador) {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_venderjugadores)

        val window = dialog.window
        window?.setBackgroundDrawableResource(R.drawable.fondo_perfilusuario)

        val btnSi = dialog.findViewById<Button>(R.id.btnSi)
        val btnNo = dialog.findViewById<Button>(R.id.btnNo)
        val textcomprar = dialog.findViewById<TextView>(R.id.venderjugadortext)
        val textdinero = dialog.findViewById<TextView>(R.id.dinerojugadortext)

        textcomprar.text = "¿ESTÁS SEGURO QUE QUIERES VENDER A ${jugador.nombre.uppercase()} POR ESTE PRECIO?"
        textdinero.text = formatPrice(jugador.precio)
        btnSi.setOnClickListener {
            val customToastView = layoutInflater.inflate(R.layout.toast_jugadorvendido, null)
            val toast = Toast(context)
            toast.duration = Toast.LENGTH_SHORT
            toast.view = customToastView
            toast.show()
            viewModel.setSelectedJugador(jugador)

            val dinero = viewModel.currentUsuario.value?.dinero

            Log.d("JUGADOR dinero", "${dinero}")

            val precioJugador = jugador.precio

            val suma = dinero?.plus(precioJugador)

            viewModel.updateDineroUsuario(suma!!)
            viewModel.updateIdUsuarioJugadorToMaquina()
            viewModel.updateJugadorAlineado(jugador.idJugador, false)



            Log.d("SUMA", "${suma}")
            viewModel.currentUsuario.value?.dinero = suma
            dialog.dismiss()
        }
        btnNo.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    fun obtenerLista(contexto: Context, clave: String): MutableList<Jugador>? {
        val prefs = contexto.getSharedPreferences("miPrefs", Context.MODE_PRIVATE)
        val gson = Gson()
        val listaComoJson = prefs.getString(clave, null)
        val tipo = object : TypeToken<MutableList<Jugador>>() {}.type
        return gson.fromJson(listaComoJson, tipo)
    }
    fun formatPrice(price: Int?): String {
        val spanishLocale = Locale("es", "ES")
        val numberFormat = NumberFormat.getNumberInstance(spanishLocale)
        return numberFormat.format(price)
    }
}