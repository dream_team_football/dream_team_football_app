package com.example.dream_team_football_app.view.fragments

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.FragmentUserBinding
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.JugadoresViewModel
import com.example.dream_team_football_app.viewmodel.PREFS_NAME
import java.text.NumberFormat
import java.util.Locale

class UserFragment : Fragment()  {
    lateinit var binding: FragmentUserBinding
    private val viewModel: JugadoresViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)
        return binding.root
    }

    fun formatPrice(price: Int?): String {
        val spanishLocale = Locale("es", "ES")
        val numberFormat = NumberFormat.getNumberInstance(spanishLocale)
        return numberFormat.format(price)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val jugadorConMasPuntos = viewModel.getJugadorConMasPuntos()
        val jugadorMasValioso = viewModel.getJugadorMasCaro()

        jugadorConMasPuntos?.let {
            Glide.with(requireContext())
                .load(it.imagenUrl)
                .into(binding.imageMejorJugadorPlantilla)
        }

        jugadorMasValioso?.let {
            Glide.with(requireContext())
                .load(it.imagenUrl)
                .into(binding.imageJugadorvalioso)
        }
        binding.nombreUsuario.text = viewModel.currentUsuario.value?.usu_username
        binding.puntosValue.text = "${viewModel.currentUsuario.value?.puntosTotales} pts"
        binding.dineroValue.text = "${formatPrice(viewModel.currentUsuario.value?.dinero)}"

        //binding.posicionTextview.text = posicion

        binding.button.setOnClickListener {
            viewModel.loginClean = true
            clearSharedPreferences()
            clearSharedPreferencesPlantilla()
            val customToastView = layoutInflater.inflate(R.layout.toast_cerrarsesion, null)
            val toast = Toast(context)
            toast.duration = Toast.LENGTH_SHORT
            toast.view = customToastView
            toast.show()
            for (i in viewModel.tusJugadoresAlinieadosList){
                viewModel.updateJugadorAlineado(i.idJugador, false)
            }
            findNavController().navigate(R.id.action_userFragment2_to_loginFragment)
        }

        binding.back.setOnClickListener {
            requireActivity().onBackPressed()
        }
        binding.cambiarContraseA.setOnClickListener {
            showChangePasswordDialog()
        }
    }
    private fun showChangePasswordDialog() {
        val dialog = Dialog(requireContext())
        dialog.setContentView(R.layout.dialog_contrasena)

        val window = dialog.window
        window?.setBackgroundDrawableResource(R.drawable.fondo_perfilusuario)

        val btnChangePassword = dialog.findViewById<Button>(R.id.btnChangePassword)
        val editTextPassword = dialog.findViewById<EditText>(R.id.editTextPassword)

        btnChangePassword.setOnClickListener {
            // Obtén la nueva contraseña del EditText
            val newPassword = editTextPassword.text.toString()
            if (newPassword.length <= 6){
                val customToastView = layoutInflater.inflate(R.layout.toast_nocontranueva, null)
                val toast = Toast(context)
                toast.duration = Toast.LENGTH_SHORT
                toast.view = customToastView
                toast.show()
            }else{
                val customToastView = layoutInflater.inflate(R.layout.toast_cambiarcontra, null)
                val toast = Toast(context)
                toast.duration = Toast.LENGTH_SHORT
                toast.view = customToastView
                toast.show()
                Log.d("CONTRASEÑAA", editTextPassword.text.toString())
                viewModel.updatePassword(newPassword)
                dialog.dismiss()
            }
        }
        dialog.show()
    }
    fun clearSharedPreferences() {
        val prefs: SharedPreferences.Editor =
            requireActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit()
        prefs.clear()
        prefs.apply()
    }

    fun clearSharedPreferencesPlantilla() {
        val prefs: SharedPreferences.Editor =
            requireActivity().getSharedPreferences("miPrefs", Context.MODE_PRIVATE).edit()
        prefs.clear()
        prefs.apply()
    }

}