package com.example.dream_team_football_app.view.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.api.Repository
import com.example.dream_team_football_app.databinding.FragmentLoginBinding
import com.example.dream_team_football_app.model.Usuario
import com.example.dream_team_football_app.view.fragments.RegisterFragment.HashUtils.hashPassword
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.JugadoresViewModel
import com.example.dream_team_football_app.viewmodel.PREFS_NAME
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginFragment : Fragment() {
    lateinit var binding: FragmentLoginBinding
    private val viewModel: JugadoresViewModel by activityViewModels()

    // Nombre del archivo de preferencias compartidas


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)

        // Recuperar datos guardados en SharedPreferences
        val prefs: SharedPreferences =
            requireActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val savedUsername = prefs.getString("username", "")
        val savedPassword = prefs.getString("password", "")
        val savedHashedPassword = prefs.getString("hashedPassword", "")
        val saveCredentials = prefs.getBoolean("saveCredentials", false)

        if (saveCredentials && savedUsername!!.isNotEmpty() && savedPassword!!.isNotEmpty()) {
            // Si hay credenciales guardadas y válidas, intenta iniciar sesión automáticamente
            attemptLogin(savedUsername, savedPassword)
        }

        // Mostrar datos guardados en los campos de texto y el estado del checkbox
        binding.usernameEdittext.setText(savedUsername)
        binding.passwordEdittext.setText(savedPassword)
        binding.saveCredentialsCheckbox.isChecked = saveCredentials

        if (viewModel.loginClean) {
            binding.usernameEdittext.text.clear()
            binding.passwordEdittext.text.clear()
            binding.saveCredentialsCheckbox.isChecked = false
            viewModel.loginClean = false
        }
        return binding.root
    }

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loginButton.setOnClickListener {
            val username = binding.usernameEdittext.text.toString()
            val password = binding.passwordEdittext.text.toString()
            val saveCredentials = binding.saveCredentialsCheckbox.isChecked
            val hashedPassword = hashPassword(password)

            if (username.isNotEmpty() && password.isNotEmpty()) {
                // Guardar datos en SharedPreferences
                val prefs: SharedPreferences.Editor =
                    requireActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit()

                if (saveCredentials) {
                    prefs.putString("username", username)
                    prefs.putString("password", password)
                }
                else {
                    prefs.remove("username")
                    prefs.remove("password")
                }

                prefs.putBoolean("saveCredentials", saveCredentials)
                prefs.apply()

                viewModel.currentUsuario.value = Usuario(0, username, hashedPassword, 0, 30000000)
                viewModel.repository = Repository(username, hashedPassword)

                CoroutineScope(Dispatchers.IO).launch {
                    val repository = Repository(username, hashedPassword)
                    val response = repository.login(viewModel.currentUsuario.value!!)

                    withContext(Dispatchers.Main) {
                        if (response.isSuccessful) {
                            val customToastView = layoutInflater.inflate(R.layout.toast_login, null)
                            val toast = Toast(context)
                            toast.duration = Toast.LENGTH_SHORT
                            toast.view = customToastView
                            toast.show()
                            CoroutineScope(Dispatchers.IO).launch {
                                viewModel.getUsuario(username)

                                withContext(Dispatchers.Main) {
                                    viewModel.success.observe(viewLifecycleOwner) { success ->
                                        if (success == true) {
                                            findNavController().navigate(R.id.action_loginFragment_to_equipoFragment)
                                        }
                                    }
                                }
                            }
                        }else{
                            val customToastView = layoutInflater.inflate(R.layout.toast_nologin, null)
                            val toast = Toast(context)
                            toast.duration = Toast.LENGTH_SHORT
                            toast.view = customToastView
                            toast.show()
                        }
                    }
                }
            }
        }
        binding.registerText.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }
    @SuppressLint("ResourceType")
    private fun attemptLogin(username: String, password: String) {
        val hashedPassword = hashPassword(password) // Hashear la contraseña ingresada

        // Obtener la contraseña hasheada almacenada en SharedPreferences
        val prefs: SharedPreferences =
            requireActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        val savedHashedPassword = prefs.getString("hashedPassword", hashedPassword)

        if (hashedPassword == savedHashedPassword) {
            // Contraseña válida, proceder con el inicio de sesión
            viewModel.currentUsuario.value = Usuario(0, username, hashedPassword, 0, 30000000)
            viewModel.repository = Repository(username, hashedPassword)

            CoroutineScope(Dispatchers.IO).launch {
                val repository = Repository(username, hashedPassword)
                val response = repository.login(viewModel.currentUsuario.value!!)

                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        CoroutineScope(Dispatchers.IO).launch {
                            viewModel.getUsuario(username)
                            withContext(Dispatchers.Main) {
                                viewModel.success.observe(viewLifecycleOwner) { success ->
                                    if (success == true) {
                                        findNavController().navigate(R.id.action_loginFragment_to_equipoFragment)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

