package com.example.dream_team_football_app.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.api.Repository
import com.example.dream_team_football_app.databinding.FragmentRegisterBinding
import com.example.dream_team_football_app.model.Usuario
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.JugadoresViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.security.MessageDigest

class RegisterFragment: Fragment() {
    lateinit var binding: FragmentRegisterBinding
    private val viewModel: JugadoresViewModel by activityViewModels()

    object HashUtils{
        fun hashPassword(password: String): String {
            val bytes = password.toByteArray()
            val md = MessageDigest.getInstance("SHA-256")
            val digest = md.digest(bytes)
            return digest.fold("") { str, it -> str + "%02x".format(it) }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.registerButton.setOnClickListener {
            val username = binding.usernameRegister.text.toString()
            val password = binding.passwordRegister.text.toString()
            val repeatPassword = binding.repeat.text.toString()

            if (username.length <= 6 || password.length <= 6) {
                val customToastView = layoutInflater.inflate(R.layout.toast_contra, null)
                val toast = Toast(context)
                toast.duration = Toast.LENGTH_SHORT
                toast.view = customToastView
                toast.show()
            } else if (password != repeatPassword) {
                val customToastView = layoutInflater.inflate(R.layout.toast_contranocoincide, null)
                val toast = Toast(context)
                toast.duration = Toast.LENGTH_SHORT
                toast.view = customToastView
                toast.show()
            }else{
                val hashedPassword = HashUtils.hashPassword(binding.passwordRegister.text.toString())
                viewModel.currentUsuario.value = Usuario(0,binding.usernameRegister.text.toString(),hashedPassword, 0, 30000000)
                viewModel.repository = Repository(binding.usernameRegister.text.toString(),hashedPassword)
                CoroutineScope(Dispatchers.IO).launch {
                    val repository = Repository(binding.usernameRegister.text.toString(), hashedPassword)
                    val response = repository.register(viewModel.currentUsuario.value!!)
                    withContext(Dispatchers.Main) {
                        if(response.isSuccessful){
                            val customToastView = layoutInflater.inflate(R.layout.toast_registrado, null)
                            val toast = Toast(context)
                            toast.duration = Toast.LENGTH_SHORT
                            toast.view = customToastView
                            toast.show()
                            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
                        }
                        else{
                            val customToastView = layoutInflater.inflate(R.layout.toast_noregistrado, null)
                            val toast = Toast(context)
                            toast.duration = Toast.LENGTH_SHORT
                            toast.view = customToastView
                            toast.show()
                        }
                    }
                }
            }
        }
        binding.atras.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
    }
}