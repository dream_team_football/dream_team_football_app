package com.example.dream_team_football_app.view.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.FragmentElegirJugadorBinding
import com.example.dream_team_football_app.adapters.AdapterElegirJugador
import com.example.dream_team_football_app.model.Jugador
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.JugadoresViewModel
import com.example.dream_team_football_app.listeners.OnClickListenerPlantilla
import com.google.gson.Gson

class ElegirJugadorFragment : Fragment(), OnClickListenerPlantilla {
    lateinit var binding: FragmentElegirJugadorBinding
    private val viewModel: JugadoresViewModel by activityViewModels()
    private lateinit var equipoAdapter: AdapterElegirJugador
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private var indice = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentElegirJugadorBinding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle = arguments
        var posi = ""
        if (bundle != null) {
            posi = bundle.getString("posicion").toString()
            indice = bundle.getInt("indice")
        }
        val listaTusJugadoresPosicion: LiveData<List<Jugador>> = Transformations.map(viewModel.tusJugadores) { listaJugadores ->
            val jugadoresFiltrados = listaJugadores?.filter { it.posicion == posi }
            if (jugadoresFiltrados.isNullOrEmpty()) {
                binding.atras.visibility = View.VISIBLE
                binding.atras.setOnClickListener {
                    findNavController().navigate(R.id.action_elegirJugadorFragment_to_equipoFragment)
                }
                binding.nojugadores.visibility = View.VISIBLE
                binding.nojugadorestext.visibility = View.VISIBLE
            }else{
                binding.atras.visibility = View.INVISIBLE
                binding.nojugadores.visibility = View.INVISIBLE
                binding.nojugadorestext.visibility = View.INVISIBLE
                binding.view.visibility = View.INVISIBLE
            }
            jugadoresFiltrados
        }
        Log.d("POSICION ORDENADA", posi)

        listaTusJugadoresPosicion.observe(viewLifecycleOwner, Observer { jugadoresFiltrados ->
            setUpRecyclerView(jugadoresFiltrados)
        })

    }

    private fun setUpRecyclerView(listOfPlayers: List<Jugador>) {
        equipoAdapter = AdapterElegirJugador(listOfPlayers, this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerViewElegirJugador.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = equipoAdapter
        }
    }


    override fun onClickPlantilla(jugador: Jugador) {
        viewModel.setSelectedJugador(jugador)
        Log.d("LISTA ALINEADOS", viewModel.tusJugadoresAlinieadosList.toString())

        val indiceJugador = viewModel.tusJugadoresAlinieadosList.indexOf(jugador)
        Log.d("INDICE JUGADOR", indiceJugador.toString())

        if (indiceJugador != -1) {
            val jugadorTemporal = viewModel.tusJugadoresAlinieadosList[indice]
            viewModel.updateJugadorAlineado(jugador.idJugador, true)
            viewModel.tusJugadoresAlinieadosList[indice] = viewModel.tusJugadoresAlinieadosList[indiceJugador]
            jugadorTemporal.alineado = true
            viewModel.tusJugadoresAlinieadosList[indiceJugador] = jugadorTemporal


        }else{
            val jugadorTemporal = viewModel.tusJugadoresAlinieadosList[indice]
            viewModel.updateJugadorAlineado(jugador.idJugador, true)
            jugador.alineado = true
            if (jugadorTemporal.idJugador != -1){
                viewModel.updateJugadorAlineado(jugadorTemporal.idJugador, false)
            }
            viewModel.tusJugadoresAlinieadosList[indice] = jugador
        }

        guardarLista(requireContext(), viewModel.tusJugadoresAlinieadosList, "jugadores_alineados")

        findNavController().navigate(R.id.action_elegirJugadorFragment_to_equipoFragment)

        Log.d("CLICK", jugador.toString())
    }



    fun guardarLista(contexto: Context, lista: MutableList<Jugador>, clave: String) {
        val prefs = contexto.getSharedPreferences("miPrefs", Context.MODE_PRIVATE)
        val editor = prefs.edit()
        val gson = Gson()
        val listaComoJson = gson.toJson(lista)
        editor.putString(clave, listaComoJson)
        editor.apply()
    }

}

