package com.example.dream_team_football_app.view.ayuda

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.FragmentAyuda6Binding
import com.example.dream_team_football_app.view.activities.MainActivity
import com.example.dream_team_football_app.viewmodel.fragment

class AyudaFragment6 : Fragment() {
    lateinit var binding: FragmentAyuda6Binding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAyuda6Binding.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        val mainActivity = requireActivity() as MainActivity
        mainActivity.setBottomNavigationVisible(false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textsiguiente.setOnClickListener {
            when (fragment){
                "clasificacion" -> findNavController().navigate(R.id.action_ayudaFragment6_to_clasificacionFragment)
                "mercado" -> findNavController().navigate(R.id.action_ayudaFragment6_to_mercadoFragment)
                "listajugadores" -> findNavController().navigate(R.id.action_ayudaFragment6_to_jugadoresListaFragment)
                "codigoliga" -> findNavController().navigate(R.id.action_ayudaFragment6_to_codigoLigaFragment)
            }
        }
        binding.textomitir.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }
}