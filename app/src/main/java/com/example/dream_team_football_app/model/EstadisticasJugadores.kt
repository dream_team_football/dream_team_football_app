package com.example.dream_team_football_app.model

data class EstadisticasJugadores (
    val idJugador: Int,
    val idJornada: Int,
    val idEstadisticas: Int,
    val minutos: Int?,
    val goles: Int?,
    val asistencias: Int?,
    val paradas: Int?,
    val tirosAPuerta: Int?,
    val balonesPerdidos: Int?,
    val valoracion: Int?
)

