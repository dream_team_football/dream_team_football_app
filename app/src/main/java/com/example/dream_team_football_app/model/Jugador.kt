package com.example.dream_team_football_app.model

data class Jugador(
    val idJugador: Int,
    val idUsuario: Int?,
    val nombre: String,
    val equipo: String,
    val shortName: String,
    val liga: String,
    val posicion: String,
    var alineado: Boolean,
    val precio: Int,
    val precioClausula: Int,
    val puntosTotales: Int,
    val imagenUrl: String,
)

