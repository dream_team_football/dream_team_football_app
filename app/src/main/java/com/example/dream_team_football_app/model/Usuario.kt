package com.example.dream_team_football_app.model

data class Usuario(
    val usu_id: Int,
    val usu_username: String,
    val usu_password: String,
    val puntosTotales: Int,
    var dinero: Int
)