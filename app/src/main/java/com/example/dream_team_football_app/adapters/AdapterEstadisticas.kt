package com.example.dream_team_football_app.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.PuntosJornadaItemBinding
import com.example.dream_team_football_app.api.Repository
import com.example.dream_team_football_app.model.EstadisticasJugadores
import com.example.dream_team_football_app.listeners.OnClickListenerJornadas

class AdapterEstadisticas(private var estadisticas: List<EstadisticasJugadores>, private val listener: OnClickListenerJornadas): RecyclerView.Adapter<AdapterEstadisticas.ViewHolder>() {
    private lateinit var context: Context
    lateinit var repository: Repository
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = PuntosJornadaItemBinding.bind(view)

        fun setListener(jornada: EstadisticasJugadores){
            binding.root.setOnClickListener{
                listener.onClick(jornada)
            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        repository = Repository("user", "password")
        val view = LayoutInflater.from(context).inflate(R.layout.puntos_jornada_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return estadisticas.size
    }

    @SuppressLint("SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val jornada = estadisticas[position]
        with(holder){
            setListener(jornada)
            binding.jornadaTextView.text = "J${jornada.idJornada}"
            binding.progressBar.progress = (20 - (jornada.valoracion?.plus(1) ?: 1)).coerceIn(1, 19)
            binding.puntos.text = jornada.valoracion?.toString()?: "-"

            when (jornada.valoracion) {
                in -50..-1 -> {
                    val color = ContextCompat.getColor(context, R.color.rojo)
                    binding.progressBar.progressBackgroundTintList = ColorStateList.valueOf(color)
                }
                in 1..4 -> {
                    val color = ContextCompat.getColor(context, R.color.amarillo)
                    binding.progressBar.progressBackgroundTintList = ColorStateList.valueOf(color)
                }
                in 5..9 -> {
                    val color = ContextCompat.getColor(context, R.color.verde)
                    binding.progressBar.progressBackgroundTintList = ColorStateList.valueOf(color)
                }
                in 10..50 -> {
                    val color = ContextCompat.getColor(context, R.color.azul)
                    binding.progressBar.progressBackgroundTintList = ColorStateList.valueOf(color)
                }
                else -> {
                    val color = ContextCompat.getColor(context, R.color.grey_bold)
                    binding.progressBar.progressBackgroundTintList = ColorStateList.valueOf(color)
                }
            }


        }
    }
}