package com.example.dream_team_football_app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.signature.ObjectKey
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.ElegirjugadorItemBinding
import com.example.dream_team_football_app.api.Repository
import com.example.dream_team_football_app.model.Jugador
import com.example.dream_team_football_app.listeners.OnClickListenerPlantilla
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AdapterElegirJugador (private var jugadores: List<Jugador>, private val listener: OnClickListenerPlantilla): RecyclerView.Adapter<AdapterElegirJugador.ViewHolder>(){

    private lateinit var context: Context
    private lateinit var repository: Repository
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ElegirjugadorItemBinding.bind(view)

        fun setListener(jugador: Jugador) {
            binding.root.setOnClickListener {
                listener.onClickPlantilla(jugador)
            }
        }

        fun updateEscudoTextView (escudo: String) {
            when (escudo) {
                "ATHLETIC CLUB" -> {
                    binding.escudoImageview.setImageResource(R.drawable.atlethic_club)
                }
                "OSASUNA" -> {
                    binding.escudoImageview.setImageResource(R.drawable.osasuna)
                }
                "ATLETICO" -> {
                    binding.escudoImageview.setImageResource(R.drawable.atleticomadrid)
                }
                "CADIZ" -> {
                    binding.escudoImageview.setImageResource(R.drawable.cadiz)
                }
                "ALAVES" -> {
                    binding.escudoImageview.setImageResource(R.drawable.alaves)
                }
                "SEVILLA" -> {
                    binding.escudoImageview.setImageResource(R.drawable.sevilla)
                }
                "VALENCIA" -> {
                    binding.escudoImageview.setImageResource(R.drawable.valencia)
                }
                "GRANADILLA" -> {
                    binding.escudoImageview.setImageResource(R.drawable.granadilla)
                }
            }
        }
        fun updatePosicion(posicion: String) {
            when (posicion) {
                "DELANTERO" -> {
                    binding.posicionTextview.text = "DL"
                    binding.posicionTextview.setBackgroundResource(R.drawable.border_position_dl)
                }
                "CENTROCAMPISTA" -> {
                    binding.posicionTextview.text = "MC"
                    binding.posicionTextview.setBackgroundResource(R.drawable.border_position_mc)
                }
                "DEFENSA" -> {
                    binding.posicionTextview.text = "DF"
                    binding.posicionTextview.setBackgroundResource(R.drawable.border_position_df)
                }
                "PORTERO" -> {
                    binding.posicionTextview.text = "POR"
                    binding.posicionTextview.setBackgroundResource(R.drawable.border_position_por)
                }
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        repository = Repository("user", "password")
        val view = LayoutInflater.from(context).inflate(R.layout.elegirjugador_item, parent, false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
        return jugadores.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val jugador = jugadores[position]

        holder.updateEscudoTextView(jugador.equipo)

        holder.updatePosicion(jugador.posicion)
        with(holder) {
            setListener(jugador)
            binding.nameTextView.text = jugador.shortName
            binding.puntosTotalesTextView.text = jugador.puntosTotales.toString()
            CoroutineScope(Dispatchers.IO).launch {
                val response = repository.getImage("/jugadores/imagenes/${jugador.nombre}.webp")
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful && response.body() != null) {
                        val foto = response.body()!!.bytes()
                        Glide.with(context)
                            .load(foto)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .signature(ObjectKey(jugador.idJugador)) // Identificador único
                            .centerCrop()
                            .into(binding.imageView)
                    }
                }
            }
        }
    }
}