package com.example.dream_team_football_app.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.signature.ObjectKey
import com.example.dream_team_football_app.api.Repository
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.ListajugadoresItemBinding
import com.example.dream_team_football_app.listeners.OnClickListener
import com.example.dream_team_football_app.model.Jugador
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.NumberFormat
import java.util.Locale

class AdapterListaJugadores(private var jugadores: List<Jugador>, private val listener: OnClickListener) :
    RecyclerView.Adapter<AdapterListaJugadores.ViewHolder>() {

    private lateinit var context: Context
    private lateinit var repository: Repository
    private var valoracionesPorJugador: Map<Int, List<String>> = emptyMap()



    @SuppressLint("NotifyDataSetChanged")
    fun updateValoracionesPorJugador(valoracionesPorJugador: Map<Int, List<String>>) {
        this.valoracionesPorJugador = valoracionesPorJugador
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ListajugadoresItemBinding.bind(view)

        fun setListener(jugador: Jugador) {
            binding.root.setOnClickListener {
                listener.onClick(jugador)
            }
            binding.filledButton.setOnClickListener {
                listener.OnButtonClicked(jugador)
            }
        }
        fun updatePosicion(posicion: String) {
            when (posicion) {
                "DELANTERO" -> {
                    binding.posicionTextview.text = "DL"
                    binding.posicionTextview.setBackgroundResource(R.drawable.border_position_dl)
                }
                "CENTROCAMPISTA" -> {
                    binding.posicionTextview.text = "MC"
                    binding.posicionTextview.setBackgroundResource(R.drawable.border_position_mc)
                }
                "DEFENSA" -> {
                    binding.posicionTextview.text = "DF"
                    binding.posicionTextview.setBackgroundResource(R.drawable.border_position_df)
                }
                "PORTERO" -> {
                    binding.posicionTextview.text = "POR"
                    binding.posicionTextview.setBackgroundResource(R.drawable.border_position_por)
                }
            }
        }


        fun updateEscudoTextView (escudo: String) {
            when (escudo) {
                "ATHLETIC CLUB" -> {
                    binding.escudoImageview.setImageResource(R.drawable.atlethic_club)
                }
                "OSASUNA" -> {
                    binding.escudoImageview.setImageResource(R.drawable.osasuna)
                }
                "ATLETICO" -> {
                    binding.escudoImageview.setImageResource(R.drawable.atleticomadrid)
                }
                "CADIZ" -> {
                    binding.escudoImageview.setImageResource(R.drawable.cadiz)
                }
                "ALAVES" -> {
                    binding.escudoImageview.setImageResource(R.drawable.alaves)
                }
                "SEVILLA" -> {
                    binding.escudoImageview.setImageResource(R.drawable.sevilla)
                }
                "VALENCIA" -> {
                    binding.escudoImageview.setImageResource(R.drawable.valencia)
                }
                "GRANADILLA" -> {
                    binding.escudoImageview.setImageResource(R.drawable.granadilla)
                }
            }
        }

        @SuppressLint("ResourceAsColor")
        fun updateBackgroundJornadas(jugadorId: Int){
            val valoraciones = valoracionesPorJugador[jugadorId]

            val jornadalist = listOf(binding.jornadaultima, binding.jornadapenultima, binding.jornadaAntepenultima)

            if (valoraciones != null) {
                for (i in valoraciones.indices) {
                    when (valoraciones[i].toIntOrNull()) {
                        in -50..-1 -> jornadalist[i].setBackgroundResource(R.drawable.border_position_dl)
                        in 1..4 -> jornadalist[i].setBackgroundResource(R.drawable.border_position_por)
                        in 5..9 -> jornadalist[i].setBackgroundResource(R.drawable.border_position_mc)
                        in 10..50 -> jornadalist[i].setBackgroundResource(R.drawable.border_position_df)
                        else -> jornadalist[i].setBackgroundResource(R.drawable.border_cero_or_null)
                    }
                }
            }

        }
        @SuppressLint("SuspiciousIndentation")
        fun updatePuntos3Jornadas(jugadorId: Int) {
            val valoraciones = valoracionesPorJugador[jugadorId]
            //Log.d("DEBUG", "updatePuntos3Jornadas: jugadorId=$jugadorId, valoraciones=$valoraciones")

            valoraciones?.let {
                binding.jornadaAntepenultima.text = it[2]
                binding.jornadapenultima.text = it[1]
                binding.jornadaultima.text = it[0]
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        repository = Repository("user", "password")
        val view = LayoutInflater.from(context).inflate(R.layout.jugador_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return jugadores.size
    }
    fun formatPrice(price: Long): String {
        val spanishLocale = Locale("es", "ES")
        val numberFormat = NumberFormat.getNumberInstance(spanishLocale)
        return numberFormat.format(price)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val jugador = jugadores[position]

        holder.updatePosicion(jugador.posicion)
        holder.updateEscudoTextView(jugador.equipo)
        holder.updateBackgroundJornadas(jugador.idJugador)

        with(holder) {
            setListener(jugador)
            binding.nameTextView.text = jugador.shortName
            binding.precioTextview.text = formatPrice(jugador.precio.toLong())
            binding.puntosTotalesTextView.text = jugador.puntosTotales.toString()
            CoroutineScope(Dispatchers.IO).launch {
                val response = repository.getImage("/jugadores/imagenes/${jugador.nombre}.webp")
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful && response.body() != null) {
                        val foto = response.body()!!.bytes()
                        Glide.with(context)
                            .load(foto)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .signature(ObjectKey(jugador.idJugador)) // Identificador único
                            .centerCrop()
                            .into(binding.imageView)

                        updatePuntos3Jornadas(jugador.idJugador)
                    }
                }
            }
        }
    }
}
