package com.example.dream_team_football_app.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.dream_team_football_app.R
import com.example.dream_team_football_app.databinding.UserItemBinding
import com.example.dream_team_football_app.model.Usuario
import com.example.dream_team_football_app.listeners.OnClickListernerUsuarios

var posicion = ""
class UserAdapter(
    private var users: List<Usuario>,
    private val listener: OnClickListernerUsuarios,
    private val currentUserId: Int,
) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = UserItemBinding.bind(view)

        fun setListener(user: Usuario) {
            binding.root.setOnClickListener {
                listener.onClickUser(user)
            }
        }
    }

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.user_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val usuario = users[position]

        with(holder) {
            setListener(usuario)
            binding.username.text = usuario.usu_username
            binding.userpuntuacion.text ="${usuario.puntosTotales} pts"
            binding.position.text = "${position + 1}º"


            if (usuario.usu_id == currentUserId) {
                binding.root.setBackgroundColor(ContextCompat.getColor(context, R.color.oro))
                posicion = "${position + 1}º"
            } else {
                binding.root.setBackgroundColor(Color.TRANSPARENT)
            }

            when (position) {
                0 -> {
                    binding.username.setTextColor(Color.parseColor("#E5BF46"))
                    binding.userpuntuacion.setTextColor(Color.parseColor("#E5BF46"))
                    binding.position.setTextColor(Color.parseColor("#E5BF46"))
                }

                1 -> {
                    binding.username.setTextColor(Color.parseColor("#b4b6b9"))
                    binding.userpuntuacion.setTextColor(Color.parseColor("#b4b6b9"))
                    binding.position.setTextColor(Color.parseColor("#b4b6b9"))
                }

                2 -> {
                    binding.username.setTextColor(Color.parseColor("#ba8445"))
                    binding.userpuntuacion.setTextColor(Color.parseColor("#ba8445"))
                    binding.position.setTextColor(Color.parseColor("#ba8445"))
                }

                else -> {
                    binding.username.setTextColor(Color.WHITE)
                    binding.userpuntuacion.setTextColor(Color.WHITE)
                    binding.position.setTextColor(Color.WHITE)
                }
            }

        }
    }
}

