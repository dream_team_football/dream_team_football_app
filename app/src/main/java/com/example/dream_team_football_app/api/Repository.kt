package com.example.dream_team_football_app.api

import com.example.dream_team_football_app.api.ApiInterface
import com.example.dream_team_football_app.model.Usuario

class Repository(username: String, password: String) {
    private val apiInterface = ApiInterface.create(username,password)

    suspend fun getJugadores(url: String) = apiInterface.getJugadores(url)
    suspend fun getEstadisticasJugadores(url: String) = apiInterface.getEstadisticasJugadores(url)
    suspend fun getEstadisticasIdJugadores(url: String) = apiInterface.getEstadisticasIdJugadores(url)
    suspend fun getJugadoresIdUsuario(idUsuario: Int) = apiInterface.getJugadoresIdUsuario(idUsuario)
    suspend fun getImage(url: String)= apiInterface.getPhoto(url)
    suspend fun register(usuario: Usuario) = apiInterface.register(usuario)
    suspend fun getUsers(url: String)= apiInterface.getUsers(url)
    suspend fun login(usuario: Usuario) = apiInterface.login(usuario)
    suspend fun getUsuario(url: String) = apiInterface.getUsuario(url)
    suspend fun getJugadoresFiltered(url: String) = apiInterface.getJugadoresFiltered(url)
    suspend fun updateIdUsuarioJugador(idJudador: Int?, idUsuario: Int? = null) = apiInterface.updateIdUsuarioJugador(idJudador, idUsuario)
    suspend fun updateDineroUsuario(idUsuario: Int, dinero: Int) = apiInterface.updateDineroUsuario(idUsuario, dinero)
    suspend fun updatePassword(idUsuario: Int, password: String) = apiInterface.updatePassword(idUsuario, password)
    suspend fun updateJugadorAlineado(idJugador: Int, alineado: Boolean) = apiInterface.updateJugadorAlineado(idJugador, alineado)

}