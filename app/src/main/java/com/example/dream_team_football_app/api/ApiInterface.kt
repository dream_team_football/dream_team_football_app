package com.example.dream_team_football_app.api

import com.burgstaller.okhttp.digest.Credentials
import com.burgstaller.okhttp.digest.DigestAuthenticator
import com.example.dream_team_football_app.model.EstadisticasJugadores
import com.example.dream_team_football_app.model.Jugador
import com.example.dream_team_football_app.model.Usuario
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Url

interface ApiInterface {
    @GET
    suspend fun getUsers(@Url url: String): Response<List<Usuario>>
    @GET
    suspend fun getJugadores(@Url url: String): Response<List<Jugador>>
    @GET
    suspend fun getEstadisticasJugadores(@Url url: String): Response<List<EstadisticasJugadores>>
    @GET
    suspend fun getEstadisticasIdJugadores(@Url url: String): Response<List<EstadisticasJugadores>>
    @GET("/jugadores/{idUsuario}")
    suspend fun getJugadoresIdUsuario(@Path("idUsuario") idUsuario: Int): Response<List<Jugador>>
    @GET
    suspend fun getPhoto(@Url url: String): Response<ResponseBody>
    @GET
    suspend fun getUsuario(@Url url: String): Response<Usuario>
    @GET
    suspend fun getJugadoresFiltered(@Url url: String): Response<List<Jugador>>

    @POST("user/login")
    suspend fun login(@Body usuario: Usuario): Response<ResponseBody>
    @POST("user/register")
    suspend fun register(@Body usuario: Usuario): Response<ResponseBody>
    @PUT("/jugadores/idJugador/{idJugador}/{idUsuario}")
    suspend fun updateIdUsuarioJugador(@Path("idJugador") idJugador: Int?, @Path("idUsuario") idUsuario: Int?): Response<ResponseBody>
    @PUT("/usuario/update/dinero/{idUsuario}/{dinero}")
    suspend fun updateDineroUsuario(@Path("idUsuario") idUsuario: Int, @Path("dinero") dinero: Int): Response<ResponseBody>
    @PUT("/usuario/update/password/{idUsuario}/{password}")
    suspend fun updatePassword(@Path("idUsuario") idUsuario: Int, @Path("password") password: String): Response<ResponseBody>
    @PUT("/jugadores/updatealineado/{idJugador}/{alineado}")
    suspend fun updateJugadorAlineado(@Path("idJugador") idJugador: Int, @Path("alineado") alineado: Boolean): Response<ResponseBody>

    companion object{
        val BASE_URL = "http://34.192.34.148:8081/"
        //url pc alex: 192.168.1.104
        //url pc jordi: 192.168.56.1
        //url pc clase alex: 172.23.6.123:8080
        //url api aws 34.192.34.148:8081
        fun create(username: String, password: String): ApiInterface {
            val digestAuthenticator = DigestAuthenticator(Credentials(username, password))

            val client = OkHttpClient.Builder()
                .authenticator(digestAuthenticator)
                .build()

            val gson = GsonBuilder().setLenient().create()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}