package com.example.dream_team_football_app.listeners


import com.example.dream_team_football_app.model.Usuario

interface OnClickListernerUsuarios {
    fun onClickUser (usuario: Usuario)

}