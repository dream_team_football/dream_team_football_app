package com.example.dream_team_football_app.listeners

import com.example.dream_team_football_app.model.Jugador

interface OnClickListenerPlantilla {
    fun onClickPlantilla (jugador: Jugador)
}