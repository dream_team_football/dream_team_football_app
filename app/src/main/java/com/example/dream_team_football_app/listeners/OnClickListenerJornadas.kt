package com.example.dream_team_football_app.listeners

import com.example.dream_team_football_app.model.EstadisticasJugadores

interface OnClickListenerJornadas {
    fun onClick(jornadas: EstadisticasJugadores)
}