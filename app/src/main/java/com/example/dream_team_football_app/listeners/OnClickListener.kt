package com.example.dream_team_football_app.listeners

import com.example.dream_team_football_app.model.Jugador

interface OnClickListener {
    fun onClick (jugador: Jugador)
    fun OnButtonClicked (jugador: Jugador)
}
