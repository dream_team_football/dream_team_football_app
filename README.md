# Dream Team Football
Realizado por Equipo de DAM: 
- [Alejandro Torres](https://gitlab.com/alejandro.torres.7e6)
- [Gerard Sanchez](https://gitlab.com/gerard.sanchez.7e6)
- [Marçal Herraiz](https://gitlab.com/marcal.herraiz.7e5)
- [Jordi Robles](https://gitlab.com/Jordiii5)

# APP
**Dream Team Football** es una plataforma revolucionaria que fusiona lo mejor del fútbol femenino y masculino en una experiencia única de juego. Con esta aplicación, los usuarios tienen la oportunidad de crear y administrar su propio equipo de fútbol mixto, seleccionando jugadores tanto de la categoría femenina como de la masculina.

La aplicación ofrece una amplia gama de funciones, desde la selección inicial de jugadores hasta la estrategia durante los partidos.Los usuarios pueden participar en ligas virtuales donde compiten contra otros equipos creados por jugadores de todo el mundo.

Una de las características más emocionantes de Dream Team Football es la posibilidad de seguir de cerca el rendimiento de los jugadores en tiempo real. Los usuarios pueden realizar cambios en su alineación, realizar compra/ventas y ajustar tu alienación según el desempeño de los jugadores durante la temporada.

Dream Team Football se compromete a impulsar la visibilidad y el reconocimiento de ambos géneros en el mundo del fútbol, promoviendo la igualdad y la inclusión en el deporte.

Con una interfaz intuitiva y emocionante, Dream Team Football ofrece una experiencia única para los aficionados al fútbol de todas partes, celebrando la diversidad y la pasión por el juego.


# Funciones usuario
## Creación de Equipos Mixtos
Los usuarios pueden crear su propio Dream Team, combinando jugadores de fútbol masculino y femenino en un equipo mixto. Pueden seleccionar y gestionar a los jugadores de su equipo.

## Mercado de Fichajes
Los usuarios tienen acceso a un mercado de fichajes donde pueden buscar, comprar y vender jugadores masculinos y femeninos para fortalecer sus equipos. Este mercado se actualiza cada día.

## Clasificación
Los usuarios pueden ver su desempeño en la creación de equipos mixtos y otras actividades dentro de la aplicación mediante una clasificación formada por puntos. Los puntos se consiguen dependiendo del rendimiento de los jugadores alineados durante las jornadas. Los usuarios pueden ver sus posiciones en la clasificación general y visualizar a los jugadores de los equipos rivales.

## Tecnologías utilizadas
- Lenguaje de Programación: [Kotlin](https://kotlinlang.org/)
- Framework: [Android Studio](https://developer.android.com/studio)
- Base de Datos: [PostgreSQL](https://www.postgresql.org/)

# Estructura utilizada
El patrón de arquitectura Modelo-Vista-ViewModel (MVVM) es el enfoque utilizado para diseñar esta aplicacion en Android Studio. En este patrón, el modelo representa los datos y la lógica de negocio de la aplicación, la vista representa la interfaz de usuario (UI) y el ViewModel actúa como un intermediario entre la vista y el modelo.

# Algunas Librerías externas
- [Lottie](http://airbnb.io/lottie/#/). Permite agregar animaciones vectoriales a la aplicación.
- [Glide](https://ed.team/blog/carga-imagenes-desde-internet-tu-proyecto-android). Esta librería es una biblioteca de carga y visualización de imágenes en Android que proporciona una API fácil de usar y una gran cantidad de opciones de configuración. 
- [Retrofit](https://programacionymas.com/blog/consumir-una-api-usando-retrofit#:~:text=C%C3%B3mo%20a%C3%B1adir%20Retrofit%20a%20nuestro%20proyecto&text=Debes%20a%C3%B1adir%20la%20dependencia%20en,gradle%20a%20nivel%20de%20m%C3%B3dulo.&text=Dentro%20del%20archivo%20debes%20agregar,Retrofit%20y%20otra%20para%20GSON.). Esta librería es una biblioteca de cliente HTTP para Android que permite realizar solicitudes a una API web y recibir respuestas en formato JSON u otros formatos.
- [Navigation Component](https://developer.android.com/jetpack/androidx/releases/navigation?hl=es-419). Esta librería es una parte de Android Jetpack que simplifica la navegación entre diferentes destinos en tu aplicación, como fragmentos, actividades y acciones personalizadas.

## Licencia
[MIT License](LICENSE)

# BackEnd
[Acceso al BackEnd](https://gitlab.com/dream_team_football/api_dtf)

# BBDD
[Acceso a la Base de datos](https://api.elephantsql.com/console/e6cf8987-1b5f-424f-a20f-b12996b58eb1/browser?#)


